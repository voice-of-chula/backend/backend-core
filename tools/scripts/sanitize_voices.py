import os
import json
import datetime

os.chdir('tools/scripts')

def get_timestamp_from_ObjectId(_id: str):
  return datetime.datetime.fromtimestamp(int(_id[:8], 16)).isoformat()

jsonFilenames = os.listdir('json')
for jsonFilename in jsonFilenames:
  newVoices = []
  with open(f'json/{jsonFilename}', mode='r', encoding='utf-8') as jsonFile:
    voices = json.load(jsonFile)
    for voice in voices:
      if '_id' in voice:
        voice['createdAt'] = get_timestamp_from_ObjectId(voice['_id']['$oid'])
        del voice['_id']
      if '__v' in voice:
        del voice['__v']
      if 'reports' in voice:
        del voice['reports']

      if isinstance(voice['upvotes'], list):
        voice['upvotes'] = len(voice['upvotes'])
      if isinstance(voice['downvotes'], list):
        voice['downvotes'] = len(voice['downvotes'])
      if len(voice['owner']) > 4:
        voice['owner'] = voice['owner'][:2] + voice['owner'][-2:]

      for reply in voice['replies']:
        if '_id' in reply:
          reply['createdAt'] = get_timestamp_from_ObjectId(reply['_id']['$oid'])
          del reply['_id']
        if len(reply['owner']) > 4:
          reply['owner'] = reply['owner'][:2] + reply['owner'][-2:]
      newVoices.append(voice)
  with open(f'json/{jsonFilename}', mode='w', encoding='utf-8') as jsonFile:
    json.dump(newVoices, jsonFile, ensure_ascii=False)