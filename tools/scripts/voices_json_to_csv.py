import os
import json
import datetime
import csv

os.chdir('tools/scripts')

faculties = {
  '21': 'คณะวิศวกรรมศาสตร์',
  '22': 'คณะอักษรศาสตร์',
  '23': 'คณะวิทยาศาสตร์',
  '24': 'คณะรัฐศาสตร์',
  '25': 'คณะสถาปัตยกรรมศาสตร์',
  '26': 'คณะพาณิชยศาสตร์และการบัญชี',
  '27': 'คณะครุศาสตร์',
  '28': 'คณะนิเทศศาสตร์',
  '29': 'คณะเศรษฐศาสตร์',
  '30': 'คณะแพทยศาสตร์',
  '31': 'คณะสัตวแพทยศาสตร์',
  '32': 'คณะทันตแพทยศาสตร์',
  '33': 'คณะเภสัชศาสตร์',
  '34': 'คณะนิติศาสตร์',
  '35': 'คณะศิลปกรรมศาสตร์',
  '36': 'คณะพยาบาลศาสตร์',
  '37': 'คณะสหเวชศาสตร์',
  '38': 'คณะจิตวิทยา',
  '39': 'คณะวิทยาศาสตร์การกีฬา',
  '40': 'สำนักวิชาทรัพยากรการเกษตร',
  '56': 'สถาบันนวัตกรรมบูรณาการ',
}

def write_voice(csvWriter: csv.DictWriter, index, voice):
  faculty = faculties[voice['owner'][2:]] if voice['owner'][2:] in faculties else f'คณะหมายเลข {voice["owner"][2:]}'
  year = 64 - int(voice['owner'][:2])
  csvWriter.writerow({
    'Voice No': index+1,
    'Topics': ', '.join(voice['topics']) if len(voice['topics']) else '-',
    'Faculty': faculty,
    'Year': f'ปี {year}',
    'Detail': voice['detail'],
    'Created At': datetime.datetime.fromisoformat(voice['createdAt']).strftime('%d-%m-%Y  %H:%M:%S')
  })
  for reply in voice['replies']:
    faculty = faculties[voice['owner'][2:]] if voice['owner'][2:] in faculties else f'คณะหมายเลข {voice["owner"][2:]}'
    year = 64 - int(voice['owner'][:2])
    csvWriter.writerow({
      'Voice No': '',
      'Topics': f'reply of {index+1}',
      'Faculty': faculty,
      'Year': f'ปี {year}',
      'Detail': reply['detail'],
      'Created At': datetime.datetime.fromisoformat(voice['createdAt']).strftime('%d-%m-%Y  %H:%M:%S')
    })


jsonFilenames = [name.split('.')[0] for name in os.listdir('json')]
for jsonFilename in jsonFilenames:
  topics = {}
  writeVoices = []
  with open(f'json/{jsonFilename}.json', mode='r', encoding='utf-8') as jsonFile:
    with open(f'csv/{jsonFilename}.csv', mode='w', encoding='utf-8', newline='') as csvFile:
      csvWriter = csv.DictWriter(csvFile, ['Voice No', 'Topics', 'Faculty', 'Year', 'Detail', 'Created At'])
      csvWriter.writeheader()
      voices = json.load(jsonFile)
      for index, voice in enumerate(voices):
        for topic in voice['topics']:
          if topic not in topics:
            topics[topic] = []
          topics[topic].append(voice)
        write_voice(csvWriter, index, voice)
  for topic in topics:
    voiceList = topics[topic]
    with open(f'csv/{jsonFilename}-{topic}.csv', mode='w', encoding='utf-8', newline='') as csvFile:
      csvWriter = csv.DictWriter(csvFile, ['Voice No', 'Topics', 'Faculty', 'Year', 'Detail', 'Created At'])
      csvWriter.writeheader()
      for index, voice in enumerate(voiceList):
        write_voice(csvWriter, index, voice)
