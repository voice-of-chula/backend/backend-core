#!/bin/python

from http.server import *
from urllib.parse import urlparse, parse_qs
from uuid import uuid4
from random import choice, randint
import json
import signal

def handle_sigterm(*args):
    raise KeyboardInterrupt()

signal.signal(signal.SIGTERM, handle_sigterm)

statefile = 'simple-cas-state.json'

tickets = {}
appId = "MyApp"
appSecret = "MySecret"

class Handler(BaseHTTPRequestHandler):

  

  def body(self):
    if 'Content-Length' not in self.headers:
      return None
    l = int(self.headers['Content-Length'])
    return self.rfile.read(l).decode('utf-8')

  def do_POST(self):
    self.params = self.body() or ""
    self.myReqHandler()
  
  def do_GET(self):
    self.params = urlparse(self.path).query
    self.myReqHandler()

  def notfound(self):
    self.send_error(404)

  def genticket(self):
    ticket = str(uuid4())
    name = choice([
      ("ประยุทธ์", "Prayuth"),
      ("ประวิตร", "Pravith"),
      ("สมคิด", "Somkid"),
      ("วิษณุ", "Vishnu"),
      ("อนุทิน", "Anutin"),
      ("อนุพงษ์", "Anupong"),
    ])

    surname = choice([
      ("จันทร์โอชา", "Jun O Cha"),
      ("วงษ์สุวรรณ", "Wongsuwan"),
      ("จาตุศรีพิทักษ์", "Jatusripitak"),
      ("เครืองาม", "Kluengam"),
      ("ชาญวีรกูล", "Chanveerakul"),
      ("เผ่าจินดา", "Poajinda"),
    ])

    sid = "62" + ''.join([str(randint(0, 9)) or "0" for _ in range(8)])

    tickets[ticket] = {
          "disable":False,
          "email": sid +"@student.chula.ac.th",
          "firstname":name[1],
          "firstnameth":name[0],
          "gecos":"{} {}, ENG".format(name[1], surname[1]),
          "lastname":surname[1],
          "lastnameth":surname[0],
          "ouid":sid,
          "roles":["student"],
          "uid":str(uuid4()),
          "username":sid
          }

    self.log_message("Generated ticket: " + ticket)
    self.log_message("===============")
    self.log_message(json.dumps(tickets[ticket]))
    return ticket

  def login(self):
    if 'service' not in self.params:
      self.log_error("No service params")
      self.send_error(400)
      return
    # Gen ticket
    self.send_response(302)
    redir = "{}?ticket={}".format(self.params['service'][0], self.genticket())
    self.send_header('Location', redir)
    self.end_headers()
    self.log_message("Redirecting to " + redir)
  
  def validation(self):
    ticket = self.headers['DeeTicket'] if 'DeeTicket' in self.headers else None
    if ('DeeAppId' not in self.headers 
        or self.headers['DeeAppId'] != appId 
        or 'DeeAppSecret' not in self.headers
        or self.headers['DeeAppSecret'] != appSecret 
        or not ticket
        or ticket not in tickets):

        self.send_response(401)
        self.send_header("content-type","application/json; charset=utf-8")
        self.end_headers()
        self.wfile.write(
          """
          {
          "type" : "error",
          "content" : "invalid ticket/permission"
          }
          """.encode())
        return
    ticket = tickets[ticket]
    self.send_response(200)
    self.send_header("content-type","application/json; charset=utf-8")
    self.end_headers()
    self.wfile.write(json.dumps(ticket).encode())

  def myReqHandler(self):
    mapping = {
      '/login': self.login,
      '/serviceValidation': self.validation
    }
    url = urlparse(self.path)
    self.params = parse_qs(self.params)
    #self.log_message("Received params " + str(self.params))
    #self.log_message("Received headers " + str(self.headers))
    if url.path in mapping:
      mapping[url.path]()
    else:
      self.notfound()

def restore_state():
  try:
    with open(statefile, 'r') as f:
      print("Loaded previous state")
      return json.load(f)
  except FileNotFoundError:
    return {}

def save_state(state):
  with open(statefile, 'w') as f:
    json.dump(state, f)
    print("Saved current state")
  


if __name__ == "__main__":
  tickets = restore_state()
  try:
    print("Server Started on port 8000")
    HTTPServer(('', 8000), Handler).serve_forever()
  except KeyboardInterrupt:
    save_state(tickets)
