#!/bin/bash

echo 'write tag :'

read TAG_NAME

IMAGE_NAME=asia.gcr.io/freshy-the-series/simple-cas:$TAG_NAME

echo IMAGE_NAME=$IMAGE_NAME

docker build -t $IMAGE_NAME .

docker push $IMAGE_NAME