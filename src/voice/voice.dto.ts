import { ApiProperty } from '@nestjs/swagger'
import { IsArray, IsString } from 'class-validator'
import { ObjectID } from 'mongodb'
import { Document } from 'mongoose'
export class voiceDto extends Document {
  @ApiProperty()
  _id: ObjectID

  @ApiProperty()
  owner: string
  @ApiProperty()
  detail: string

  @ApiProperty()
  reports?: string[]
  @ApiProperty()
  upvotes?: string[]
  @ApiProperty()
  downvotes?: string[]

  @ApiProperty()
  topics: string[]

  @ApiProperty()
  replies?: { _id: ObjectID; owner: string; detail: string }[]

  @ApiProperty()
  reportCount?: number
}

export class replyDto extends Document {
  @ApiProperty()
  owner: string

  @ApiProperty()
  createdTime: Date

  @ApiProperty()
  detail: string
}

export class resultVoiceDto {
  @ApiProperty()
  _id: ObjectID

  @ApiProperty()
  owner: string
  @ApiProperty()
  detail: string

  @ApiProperty()
  upvotes: number
  @ApiProperty()
  downvotes: number

  @ApiProperty()
  selfUpvoted: boolean
  @ApiProperty()
  selfDownvoted: boolean

  @ApiProperty()
  selfReported: boolean

  @ApiProperty()
  topics: string[]

  @ApiProperty()
  replies?: { _id: ObjectID; owner: string; detail: string }[]

  @ApiProperty()
  replyCount: number

  @ApiProperty()
  createdTime: Date

  @ApiProperty()
  reportCount: number
}

export class createVoiceDto {
  @ApiProperty()
  @IsString()
  detail: string

  @ApiProperty()
  @IsString({ each: true })
  @IsArray()
  topics: string[]
}

export class replyVoiceDto {
  @ApiProperty()
  @IsString()
  detail: string
}

export class upvoteDto {
  @ApiProperty()
  upvote: boolean
  @ApiProperty()
  downvote: boolean
}

export class voiceCountDto {
  @ApiProperty()
  count: number
}
