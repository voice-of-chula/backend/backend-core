import { promisify } from 'util'
import { Injectable } from '@nestjs/common'
import { InjectConnection } from '@nestjs/mongoose'
import { Connection, Types } from 'mongoose'
import { RedisClient } from 'redis'
import { LoggingService } from 'src/logging/logging.service'
import Logger from 'bunyan'
import { getVoiceModelFromConnection } from '../schemas/voice.schema'
import { voiceDto } from './voice.dto'

const sortedScoreCacheKey = (proposalId: string) => `VOICESORTED:${proposalId}`

@Injectable()
export class VoiceRankingService {
  logger: Logger

  constructor(
    readonly redis: RedisClient,
    @InjectConnection() readonly mongoConnection: Connection,
    readonly loggingSrv: LoggingService,
  ) {
    this.logger = loggingSrv.getLogger()
  }

  private redisPromise(fn) {
    return promisify(fn).bind(this.redis)
  }

  /**
   * Calculate score of the given voice
   */
  private score(voice: voiceDto) {
    const interactCnt =
      (voice.upvotes?.length || 0) -
      (voice.downvotes?.length || 0) /*+ (voice.replies?.length || 0)*/
    const interactScr = Math.log(Math.max(interactCnt, 1)) //Base e 8.6 hr for first vote, 5 hr for second vote
    const freshnessScr =
      (voice._id.getTimestamp().getTime() - 1618160400000) /
      (12.5 * 60 * 60 * 1000)
    return interactScr + freshnessScr
  }

  /**
   * Rebuild entire voice ranking set
   */
  async rebuildSet(proposalId: string) {
    this.logger.warn(`Rebuilding index for ${proposalId}`)
    const startTime = Date.now()
    const voiceModel = getVoiceModelFromConnection(
      this.mongoConnection,
      proposalId,
    )
    const voices = await voiceModel.find({})
    await Promise.all(
      voices.map((v) => this.recomputeVoiceScoreCache(v, proposalId)),
    )
    this.logger.warn(
      `Rebuilding index for ${proposalId} took ${
        Date.now() - startTime
      } milliseconds`,
    )
  }

  /**
   * Rebuild voice ranking set for the specific voice
   * @param voice
   */
  async recomputeVoiceScoreCache(voice: voiceDto, proposalId: string) {
    const voiceId = voice._id.toHexString()
    const scr = this.score(voice)
    await this.redisPromise(this.redis.zadd)(
      sortedScoreCacheKey(proposalId),
      scr,
      voiceId,
    )
  }

  /**
   * Remove the voice from ranking set
   * @param voiceId
   * @param proposalId
   */
  async eraseVoiceFromCache(voiceId: Types.ObjectId, proposalId: string) {
    await this.redisPromise(this.redis.zrem)(
      sortedScoreCacheKey(proposalId),
      voiceId.toHexString(),
    )
  }

  /**
   * Get the top-n scored voices. If afterVoiceId is specified, it will return top-n voices with rank lower than that voice
   * @param limit upper limit of no. of items to return
   * @param afterVoiceId if provided, will start from this voice offset. if the voice doesn't exist in the cache, this parameter will be ignored
   */
  async getRankedVoice(
    proposalId: string,
    limit: number,
    afterVoiceId?: Types.ObjectId,
  ) {
    const setKey = sortedScoreCacheKey(proposalId)
    const cacheExist: number = await this.redisPromise(this.redis.exists)(
      setKey,
    )
    if (!cacheExist) await this.rebuildSet(proposalId)

    let offset = 0
    if (afterVoiceId) {
      const voiceRank = await this.redisPromise(this.redis.zrevrank)(
        setKey,
        afterVoiceId.toHexString(),
      )
      if (voiceRank) offset = voiceRank + 1
    }

    const result: Types.ObjectId[] = (
      await this.redisPromise(this.redis.zrevrange)(
        setKey,
        offset,
        offset + limit,
      )
    ).map((s: string) => Types.ObjectId(s))
    const voiceModel = getVoiceModelFromConnection(
      this.mongoConnection,
      proposalId,
    )
    const voicesSet = await voiceModel.find({ _id: { $in: result } })
    return result.map((vid) => voicesSet.find((c) => c._id.equals(vid)))
  }
}
