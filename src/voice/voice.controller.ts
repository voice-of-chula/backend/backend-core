import {
  BadRequestException,
  Body,
  Controller,
  DefaultValuePipe,
  Delete,
  ForbiddenException,
  Get,
  GoneException,
  Param,
  ParseBoolPipe,
  ParseIntPipe,
  Patch,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiGoneResponse,
  ApiHeader,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiQuery,
  ApiSecurity,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger'
import { isMongoId } from 'class-validator'
import { JwtPayload } from 'src/auth/auth.dto'
import { UserAuthGuard } from 'src/auth/user-auth.guard'
import { FunctionalValidatorPipe } from 'src/functional-validator.pipe'
import { User } from 'src/auth/user.decorator'
import { ReqLogger } from 'src/logging/logging.decorator'
import Logger from 'bunyan'
import { LogKind } from 'src/logging/logging.service'
import * as crc from 'crc'
import { AdminGuard } from '../auth/admin.guard'
import { ProposalIdGuard } from './voice.guard'
import { VoiceService, convertVoice } from './voice.service'
import {
  createVoiceDto,
  replyDto,
  replyVoiceDto,
  resultVoiceDto,
  voiceCountDto,
} from './voice.dto'

@Controller('proposal/:proposalId')
@ApiTags('Voice')
@ApiForbiddenResponse({
  description: 'Dont have this proposalId',
})
@UseGuards(ProposalIdGuard)
export class VoiceController {
  // public readonly MAX_VOICE_QUERY_NUM = 100
  // public readonly MAX_VOICE_TOPIC = 3
  // public readonly MAX_VOICE_CHARLEN = 5000
  // public readonly MAX_REPLY_CHARLEN = 1000
  constructor(
    private voiceService: VoiceService,
    private configService: ConfigService,
  ) {}

  @Get('voice/count')
  @ApiOperation({
    summary:
      'get all voices count in a proposal (put in proposalId = “all” for all proposals)',
  })
  @ApiOkResponse({ type: voiceCountDto, description: 'OK' })
  async getCount(@Param('proposalId') proposalId: string) {
    const count = await this.voiceService.getCount(proposalId)
    return { count }
  }

  @Get('voice')
  @ApiOperation({
    summary: 'get all voices in a proposal filtered not exist',
  })
  @ApiOkResponse({ type: [resultVoiceDto], description: 'OK' })
  @ApiBadRequestResponse({ description: 'Voice query overlimit' })
  @ApiSecurity('access token')
  @UseGuards(UserAuthGuard({ userNullable: true }))
  @ApiQuery({ required: false, name: 'afterVoiceId' })
  async readmore(
    @Param('proposalId') proposalId: string,
    @Query(
      'limit',
      new DefaultValuePipe('50'),
      ParseIntPipe,
      new FunctionalValidatorPipe((val: number) => val <= 100),
    )
    limit: number,
    @Query('afterVoiceId', new FunctionalValidatorPipe(isMongoId, true))
    voiceId: string,
    @User() user: JwtPayload,
  ) {
    return (
      await this.voiceService.getVoices(proposalId, limit, voiceId)
    ).map((voice) => convertVoice(voice, user?.ouid))
  }

  @Get('reportedVoice')
  @UseGuards(AdminGuard)
  @ApiTags('admin')
  @ApiHeader({ name: 'x-admin-token', required: true })
  @ApiOperation({
    summary: 'get all reported voices in a proposal',
  })
  @ApiOkResponse({ type: [resultVoiceDto], description: 'OK' })
  async getReportedVoices(@Param('proposalId') proposalId: string) {
    return (
      await this.voiceService.getReportedVoices(proposalId)
    ).map((voice) => convertVoice(voice))
  }

  @Get('topicQuery')
  @ApiOperation({
    summary: 'get all voices in a proposal filtered by topic',
  })
  @ApiSecurity('access token')
  @UseGuards(UserAuthGuard({ userNullable: true }))
  @ApiOkResponse({ type: [resultVoiceDto], description: 'OK' })
  @ApiBadRequestResponse({ description: 'Voice query overlimit' })
  async getVoicesByTopic(
    @Query('topic') topic: string,
    @Param('proposalId') proposalId: string,
    @Query(
      'limit',
      new DefaultValuePipe('50'),
      ParseIntPipe,
      new FunctionalValidatorPipe((val: number) => val <= 100),
    )
    limit: number,
    @Query('afterVoiceId', new FunctionalValidatorPipe(isMongoId, true))
    voiceId: string,
    @User() user: JwtPayload,
  ) {
    return (
      await this.voiceService.getVoicesByTopic(
        topic,
        proposalId,
        limit,
        voiceId,
      )
    ).map((voice) => convertVoice(voice, user?.ouid))
  }

  @Get('voice/me')
  @ApiOperation({
    summary: 'get my voice [protected]',
  })
  @ApiSecurity('access token')
  @UseGuards(UserAuthGuard())
  @ApiOkResponse({ type: [resultVoiceDto], description: 'OK' })
  @ApiBadRequestResponse({ description: 'Voice query overlimit' })
  async getMyVoice(
    @Param('proposalId') proposalId: string,
    @Query(
      'limit',
      new DefaultValuePipe('50'),
      ParseIntPipe,
      new FunctionalValidatorPipe((val: number) => val <= 100),
    )
    limit: number,
    @User() user: JwtPayload,
  ) {
    return (
      await this.voiceService.getMyVoice(user.ouid, proposalId, limit)
    ).map((voice) => convertVoice(voice, user.ouid))
  }

  @Get('voice/:voiceId')
  @ApiOperation({ summary: 'get a single voice' })
  @ApiSecurity('access token')
  @UseGuards(UserAuthGuard({ userNullable: true }))
  @ApiBadRequestResponse({ description: 'put ?before | ?after' })
  @ApiOkResponse({ type: resultVoiceDto, description: 'OK' })
  async getVoice(
    @Param('voiceId') voiceId: string,
    @Param('proposalId') proposalId: string,
    @User() user: JwtPayload,
  ) {
    return this.voiceService.getAVoiceByVoiceId(proposalId, voiceId, user?.ouid)
  }

  @Get('quarantinedVoice/:voiceId')
  @UseGuards(AdminGuard)
  @ApiTags('admin')
  @ApiHeader({ name: 'x-admin-token', required: true })
  @ApiOperation({ summary: 'get quarantine voice by id' })
  @ApiOkResponse({
    description: 'Got the quarantined voice',
    type: resultVoiceDto,
  })
  @ApiGoneResponse({ description: 'Quarantine voice no longer here' })
  @ApiNotFoundResponse({ description: 'Admin token not found or invalid' })
  async getQuarantinedVoiceById(
    @Param('voiceId') voiceId: string,
    @Param('proposalId') proposalId: string,
    @ReqLogger() logger: Logger,
  ) {
    logger.info(
      { kind: LogKind.ADMINACTION, voiceId, proposalId },
      'Admin access quarantined voice',
    )
    const voice = await this.voiceService.getQuarantinedVoice(proposalId, {
      _id: voiceId,
    })
    if (voice.length == 0) {
      throw new GoneException()
    }
    return convertVoice(voice[0])
  }

  @Get('quarantinedVoice')
  @UseGuards(AdminGuard)
  @ApiTags('admin')
  @ApiHeader({ name: 'x-admin-token', required: true })
  @ApiOperation({ summary: 'get quarantined voices' })
  @ApiOkResponse({
    description: 'Got quarantined voices',
    type: [resultVoiceDto],
  })
  @ApiNotFoundResponse({ description: 'Admin token not found or invalid' })
  async getQuarantinedVoices(
    @Param('proposalId') proposalId: string,
    @ReqLogger() logger: Logger,
  ) {
    logger.info(
      { kind: LogKind.ADMINACTION, proposalId },
      'Admin access quarantined voices in proposal',
    )
    return await (
      await this.voiceService.getQuarantinedVoice(proposalId, {})
    ).map((v) => convertVoice(v))
  }

  @Get('voice/:voiceId/reply')
  @ApiOperation({ summary: 'readmore reply' })
  @ApiBadRequestResponse({ description: 'put ?before | ?after' })
  @ApiSecurity('access token')
  @ApiOkResponse({ type: [replyDto], description: 'OK' })
  @ApiBadRequestResponse({ description: 'Reply query overlimit' })
  async readMoreReply(
    @Param('voiceId') voiceId: string,
    @Param('proposalId') proposalId: string,
    @Query(
      'limit',
      new DefaultValuePipe('50'),
      ParseIntPipe,
      new FunctionalValidatorPipe((val: number) => val <= 100),
    )
    limit: number,
    @Query('before') replyIdBefore: string,
    @Query('after') replyIdAfter: string,
  ) {
    return this.voiceService.readMoreReply(
      proposalId,
      voiceId,
      replyIdBefore,
      replyIdAfter,
      limit,
    )
  }
  //Protected methods need to use jwt

  @Post('voice')
  @UseGuards(UserAuthGuard())
  @ApiSecurity('access token')
  @ApiOperation({ summary: 'create voice [ protected ]' })
  @ApiOkResponse({ type: resultVoiceDto, description: 'OK' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiBadRequestResponse({
    description: `Either: voice detail too long, too much topics, voice detail is empty, or a topic(s) in the array is empty`,
  })
  async createVoice(
    @Body(
      new FunctionalValidatorPipe(
        (data: createVoiceDto) =>
          data.detail && // check if empty detail
          data.detail.length <= 5000 &&
          data.topics.length <= 3 &&
          !data.topics.some((t, i, ts) => t === ''), // return false if any of topic(s) is empty
      ),
    )
    data: createVoiceDto,
    @User() user: JwtPayload,
    @Param('proposalId') proposalId: string, // TODO: use proposalId to create voice
    @ReqLogger() logger: Logger,
  ) {
    const result = await this.voiceService.createVoice(
      data,
      user.ouid,
      proposalId,
    )
    logger.info(
      {
        voiceId: result._id.toHexString(),
        proposalId,
        messageCRC: crc.crc32(result.detail), //Limit log size
        kind: LogKind.USERCONTENT,
      },
      'User post voice',
    )
    return result
  }

  @Delete('voice/:voiceId')
  @UseGuards(UserAuthGuard())
  @ApiSecurity('access token')
  @ApiOperation({ summary: 'delete voice [ protected ]' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiForbiddenResponse({
    description: "Didn't have permission to delete or Not found Voice ID",
  })
  @ApiOkResponse({ description: 'OK , deleted' })
  async deleteVoice(
    @Param('voiceId') voiceId: string,
    @User() user: JwtPayload,
    @Param('proposalId') proposalId: string,
    @ReqLogger() logger: Logger,
  ) {
    if (await this.voiceService.deleteVoice(voiceId, user.ouid, proposalId)) {
      logger.info(
        {
          voiceId: voiceId,
          proposalId,
          kind: LogKind.USERCONTENT,
        },
        'User delete voice',
      )
      return { success: true }
    }
    throw new ForbiddenException(
      "Didn't have permission to delete or Not found Voice ID",
    )
  }

  @Delete('voice/:voiceId/quarantine')
  @UseGuards(AdminGuard)
  @ApiTags('admin')
  @ApiHeader({ name: 'x-admin-token', required: true })
  @ApiOperation({ summary: 'quarantine voice' })
  @ApiNotFoundResponse({ description: 'Admin token not found or invalid' })
  async quarantineVoice(
    @Param('voiceId') voiceId: string,
    @Param('proposalId') proposalId: string,
    @ReqLogger() logger: Logger,
  ) {
    logger.info(
      { kind: LogKind.ADMINACTION, action: 'quarantine', voiceId, proposalId },
      'Admin quarantine voice',
    )
    return convertVoice(
      await this.voiceService.quarantineVoiceById(proposalId, voiceId),
    )
  }

  @Patch('voice/:voiceId/unquarantine')
  @UseGuards(AdminGuard)
  @ApiTags('admin')
  @ApiHeader({ name: 'x-admin-token', required: true })
  @ApiOperation({ summary: 'unquarantine voice' })
  @ApiNotFoundResponse({ description: 'Admin token not found or invalid' })
  async unquarantineVoice(
    @Param('voiceId') voiceId: string,
    @Param('proposalId') proposalId: string,
    @ReqLogger() logger: Logger,
  ) {
    logger.info(
      {
        kind: LogKind.ADMINACTION,
        action: 'unquarantine',
        voiceId,
        proposalId,
      },
      'Admin unquarantine voice',
    )
    return convertVoice(
      await this.voiceService.unQuarantineVoiceById(proposalId, voiceId),
    )
  }

  @Put('voice/:voiceId/report')
  @UseGuards(UserAuthGuard())
  @ApiSecurity('access token')
  @ApiOperation({ summary: 'report voice [protected]' })
  @ApiOkResponse({ description: 'OK , reported' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  async report(
    @Param('voiceId') voiceId: string,
    @User() user: JwtPayload,
    @Param('proposalId') proposalId: string,
  ) {
    await this.voiceService.report(voiceId, user.ouid, proposalId)
    return { success: true }
  }

  @Put('voice/:voiceId/upvote')
  @UseGuards(UserAuthGuard())
  @ApiSecurity('access token')
  @ApiOperation({ summary: 'upvote voice [protected]' })
  @ApiOkResponse({ description: 'OK', type: resultVoiceDto })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiBadRequestResponse({ description: "Don't have this voiceId" })
  async upvote(
    @Param('voiceId') voiceId: string,
    @User() user: JwtPayload,
    @Param('proposalId') proposalId: string,
    @Body('upvote', ParseBoolPipe) isUpvote: boolean,
  ) {
    try {
      if (isUpvote) {
        return await this.voiceService.upvote(voiceId, user.ouid, proposalId)
      } else {
        return await this.voiceService.unvote(voiceId, user.ouid, proposalId)
      }
    } catch (e) {
      if (e instanceof GoneException) {
        throw new BadRequestException("Don't have this voiceId")
      } else {
        throw e
      }
    }
  }

  @Put('voice/:voiceId/downvote')
  @UseGuards(UserAuthGuard())
  @ApiSecurity('access token')
  @ApiOperation({ summary: 'downvote voice [protected]' })
  @ApiOkResponse({ description: 'OK', type: resultVoiceDto })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiBadRequestResponse({ description: "Don't have this voiceId" })
  async downvote(
    @Param('voiceId') voiceId: string,
    @User() user: JwtPayload,
    @Param('proposalId') proposalId: string,
    @Body('downvote', ParseBoolPipe) isDownvote: boolean,
  ) {
    try {
      if (isDownvote) {
        return await this.voiceService.downvote(voiceId, user.ouid, proposalId)
      } else {
        return await this.voiceService.unvote(voiceId, user.ouid, proposalId)
      }
    } catch (e) {
      if (e instanceof GoneException) {
        throw new BadRequestException("Don't have this voiceId")
      } else {
        throw e
      }
    }
  }

  @Post('voice/:voiceId/reply')
  @UseGuards(UserAuthGuard())
  @ApiSecurity('access token')
  @ApiOperation({ summary: 'create reply [protected]' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiBadRequestResponse({
    description: "Don't have this voiceId or Reply detail too long",
  })
  @ApiCreatedResponse({ description: 'OK , reply', type: resultVoiceDto })
  async reply(
    @Param('voiceId') voiceId: string,
    @Body(
      new FunctionalValidatorPipe(
        (data: replyVoiceDto) =>
          data.detail && // check if empty detail
          data.detail.length <= 1000,
      ),
    )
    reply: replyVoiceDto,
    @User() user: JwtPayload,
    @Param('proposalId') proposalId: string,
    @ReqLogger() logger: Logger,
  ) {
    const owner = user.ouid
    const voice = await this.voiceService.reply(
      voiceId,
      reply.detail,
      owner,
      proposalId,
    )
    logger.info(
      {
        voiceId,
        proposalId,
        replyCRC: crc.crc32(reply.detail), //Limit log size
        kind: LogKind.USERCONTENT,
      },
      'User reply voice',
    )
    if (voice) {
      return voice
    }
    throw new BadRequestException("Don't have this voiceId")
  }
}
