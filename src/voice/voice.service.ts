import { BadRequestException, GoneException, Injectable } from '@nestjs/common'
import { InjectConnection } from '@nestjs/mongoose'
import { ObjectId } from 'mongodb'
import { Connection, Model, MongooseFilterQuery, Types } from 'mongoose'
import VoiceSchema, {
  VOICE_PREFIX,
  getVoiceModelFromConnection,
} from 'src/schemas/voice.schema'
import { createVoiceDto, resultVoiceDto, voiceDto } from './voice.dto'
import { VoiceRankingService } from './voiceranking.service'

const VOICE_QUARANTINED_PREFIX = 'quarantined-voice-'

interface PopularVoiceCache {
  [proposalId: string]: voiceDto[]
}

@Injectable()
export class VoiceService {
  //-----------------------------------------------------------------------------
  // constructor
  //-----------------------------------------------------------------------------
  public readonly POP_VOICE_NUM: number = 3
  constructor(
    @InjectConnection() readonly connection: Connection,
    readonly voiceRankSrv: VoiceRankingService,
  ) {}
  //-----------------------------------------------------------------------------
  // to get voice Model
  //-----------------------------------------------------------------------------
  getVoiceModel(proposalId: string): Model<voiceDto> {
    return getVoiceModelFromConnection(this.connection, proposalId)
  }

  private getQuarantinedVoiceModel(proposalId: string): Model<voiceDto> {
    return this.connection.model(
      VOICE_QUARANTINED_PREFIX + proposalId,
      VoiceSchema,
      VOICE_QUARANTINED_PREFIX + proposalId,
    )
  }
  //-----------------------------------------------------------------------------
  // service for count voice
  //-----------------------------------------------------------------------------

  async getCount(proposalId: string): Promise<number> {
    // if proposalId == all this method return all count
    if (proposalId === 'all') {
      return this.getAllCount()
    }
    return await this.getVoiceModel(proposalId).estimatedDocumentCount()
  }

  async getAllCount(): Promise<number> {
    //to get all proposal collection
    const collections: any[] = await this.connection.db
      .listCollections()
      .toArray()
    const voiceCollections = collections.filter((name) =>
      name['name'].startsWith(VOICE_PREFIX),
    )

    //to count
    const cnt = (
      await Promise.all(
        voiceCollections.map((col) =>
          this.connection.collection(col['name']).estimatedDocumentCount(),
        ),
      )
    ).reduce((a, b) => a + b, 0)
    return cnt
  }

  async countVoiceByTopic(topic: string, proposalId: string) {
    return await this.getVoiceModel(proposalId).aggregate([
      /* first stage - matching */
      {
        $match: {
          topics: topic,
        },
      },
      /* second stage - distinct owner by $group command */
      // {
      //   $group: {
      //     _id: '$owner',
      //   },
      // },
      /* last stage - counting all owners, to avoid using excessive memory on db side */
      {
        $count: 'count',
      },
    ])
  }

  //-----------------------------------------------------------------------------
  // service for CRUD
  //-----------------------------------------------------------------------------

  //create voice
  async createVoice(data: createVoiceDto, userId: string, proposalId: string) {
    const owner: string = userId
    const voice = { owner, ...data }
    const createdVoice = await this.getVoiceModel(proposalId).create(
      voice as any,
    )

    this.voiceRankSrv.recomputeVoiceScoreCache(createdVoice, proposalId)
    return convertVoice(createdVoice, owner)
  }
  //delete voice
  async deleteVoice(voiceId: string, owner: string, proposalId: string) {
    const voice = await this.getVoiceModel(proposalId).findById(voiceId)
    if (voice) {
      if (voice.owner === owner) {
        await this.getVoiceModel(proposalId).deleteOne(voice)
        await this.voiceRankSrv.eraseVoiceFromCache(
          Types.ObjectId(voiceId),
          proposalId,
        )
        return true
      }
    }
    return false
  }
  // upvote voice
  async upvote(voiceId: string, userId: string, proposalId: string) {
    const result = await this.getVoiceModel(proposalId).findByIdAndUpdate(
      voiceId,
      {
        $addToSet: { upvotes: userId },
        $pull: { downvotes: userId },
      },
      { new: true },
    )

    if (!result) {
      // return { status: false, action: '-' }
      throw new GoneException(
        "Either this voice got quarantined by the admin or there isn't this voiceId at the first place",
      )
    }
    this.voiceRankSrv.recomputeVoiceScoreCache(result, proposalId)

    // return { status: true, upvote: true }
    return convertVoice(result, userId)
  }

  // downvote voice
  async downvote(voiceId: string, userId: string, proposalId: string) {
    const result = await this.getVoiceModel(proposalId).findByIdAndUpdate(
      voiceId,
      {
        $pull: { upvotes: userId },
        $addToSet: { downvotes: userId },
      },
      { new: true },
    )

    if (!result) {
      // return { status: false, action: '-' }
      throw new GoneException(
        "Either this voice got quarantined by the admin or there isn't this voiceId at the first place",
      )
    }
    this.voiceRankSrv.recomputeVoiceScoreCache(result, proposalId)

    // return { status: true, upvote: true }
    return convertVoice(result, userId)
  }

  // unvote voice
  async unvote(voiceId: string, userId: string, proposalId: string) {
    const result = await this.getVoiceModel(proposalId).findByIdAndUpdate(
      voiceId,
      {
        $pull: { upvotes: userId, downvotes: userId },
      },
      { new: true },
    )

    if (!result) {
      // return { status: false, action: '-' }
      throw new GoneException(
        "Either this voice got quarantined by the admin or there isn't this voiceId at the first place",
      )
    }

    this.voiceRankSrv.recomputeVoiceScoreCache(result, proposalId)

    // return { status: true, upvote: false }
    return convertVoice(result, userId)
  }
  // report voice
  async report(voiceId: string, userId: string, proposalId: string) {
    await this.getVoiceModel(proposalId).findByIdAndUpdate(voiceId, {
      $addToSet: { reports: userId },
    })
  }

  async getReportedVoices(proposalId: string) {
    return this.getVoiceModel(proposalId).aggregate([
      { $match: { 'reports.0': { $exists: true } } },
      { $addFields: { reportCount: { $size: '$reports' } } },
      { $sort: { reports: 1 } },
    ])
  }
  // reply  voice
  async reply(
    voiceId: string,
    detail: string,
    owner: string,
    proposalId: string,
  ) {
    const voice = await this.getVoiceModel(proposalId).findByIdAndUpdate(
      voiceId,
      { $push: { replies: { owner, detail } } },
      { new: true },
    )

    if (voice) {
      this.voiceRankSrv.recomputeVoiceScoreCache(voice, proposalId)
      return convertVoice(voice, owner)
    }
    return false
  }

  //-----------------------------------------------------------------------------
  // service for get voice
  //-----------------------------------------------------------------------------
  async getVoicesByTopic(
    topic: string,
    proposal: string,
    limit: number,
    voiceId?: string,
  ): Promise<voiceDto[]> {
    if (voiceId) {
      return this.readMoreVoiceByTopic(proposal, voiceId, topic, limit)
    }
    return this.getVoiceModel(proposal)
      .find({ topics: { $all: [topic] } })
      .sort({ _id: -1 })
      .limit(limit)
  }

  async getVoices(proposal: string, limit: number, voiceId?: string) {
    return this.voiceRankSrv.getRankedVoice(
      proposal,
      limit,
      voiceId ? Types.ObjectId(voiceId) : undefined,
    )
  }

  async getMyVoice(owner: string, proposalId: string, limit: number) {
    return this.getVoiceModel(proposalId)
      .find({ owner: owner })
      .sort({ _id: -1 })
      .limit(limit)

    // .limit(limit)
  }

  async readMoreVoiceByTopic(
    proposalId: string,
    voiceId: string,
    topic: string,
    limit: number,
  ) {
    // console.log(user, voiceId, topic);

    return this.getVoiceModel(proposalId)
      .find({
        topics: { $all: [topic] },
        _id: { $lt: new Types.ObjectId(voiceId) },
      })
      .sort({ _id: -1 })
      .limit(limit)
  }

  async getAVoiceByVoiceId(proposalId: string, voiceId: string, owner: string) {
    const voice = await this.getVoiceModel(proposalId).findById(voiceId)
    return convertVoice(voice, owner)
  }

  async getQuarantinedVoice(
    proposalId: string,
    cond: MongooseFilterQuery<
      Pick<
        voiceDto,
        | '_id'
        | 'owner'
        | 'detail'
        | 'reports'
        | 'upvotes'
        | 'topics'
        | 'replies'
        | 'reportCount'
      >
    >,
  ) {
    return await this.getQuarantinedVoiceModel(proposalId).find(cond)
  }

  async quarantineVoiceById(proposalId: string, voiceId: string) {
    const voiceModel = this.getVoiceModel(proposalId)
    const quarantinedVoiceModel = this.getQuarantinedVoiceModel(proposalId)

    const targetVoice = await voiceModel.findById(voiceId)

    if (targetVoice == null) {
      throw new GoneException(
        "Either this voice got quarantined by the admin or there isn't this voiceId at the first place",
      )
    }

    const { __v, ...voice } = targetVoice.toObject()

    await quarantinedVoiceModel.create(voice)
    await voiceModel.remove({ _id: new Types.ObjectId(voiceId) })
    await this.voiceRankSrv.eraseVoiceFromCache(
      Types.ObjectId(voiceId),
      proposalId,
    )
    return voice
  }

  async unQuarantineVoiceById(proposalId: string, voiceId: string) {
    const voiceModel = this.getVoiceModel(proposalId)
    const quarantinedVoiceModel = this.getQuarantinedVoiceModel(proposalId)

    const targetVoice = await quarantinedVoiceModel.findById(voiceId)

    if (targetVoice == null) {
      throw new GoneException(
        "Either this voice got quarantined by the admin or there isn't this voiceId at the first place",
      )
    }

    const { __v, ...voice } = targetVoice.toObject()

    const createdVoice = await voiceModel.create(voice)
    await this.voiceRankSrv.recomputeVoiceScoreCache(createdVoice, proposalId)
    await quarantinedVoiceModel.remove({ _id: new Types.ObjectId(voiceId) })
    return voice
  }

  //-----------------------------------------------------------------------------
  // service for Proposal
  //-----------------------------------------------------------------------------
  async getAllTopic(proposalId: string) {
    return (await this.getVoiceModel(proposalId).distinct('topics')).filter(
      (topic) => topic != undefined,
    )
  }

  //-----------------------------------------------------------------------------
  // methods for reply
  //-----------------------------------------------------------------------------
  async readMoreReply(
    proposalId: string,
    voiceId: string,
    replyIdBefore: string,
    replyIdAfter: string,
    limit: number,
  ) {
    if (!(replyIdBefore || replyIdAfter))
      return new BadRequestException('put ? before | ? after')

    const replies = (await this.getVoiceModel(proposalId).findById(voiceId))
      .replies
    let result = []
    if (replyIdBefore) {
      result = result.concat(
        replies.filter((e) => {
          return new ObjectId(e._id) < new ObjectId(replyIdBefore)
        }),
      )
    }

    if (replyIdAfter) {
      result = result.concat(
        replies.filter((e) => {
          return new ObjectId(e._id) > new ObjectId(replyIdAfter)
        }),
      )
    }

    result = result.slice(-limit)
    result = result.map(({ _id, owner, detail }) => ({
      _id: _id,
      detail: detail,
      owner: `${owner.slice(0, 2)}${owner.slice(-2)}`,
      createdTime: new Types.ObjectId(_id).getTimestamp(),
    }))

    return result
  }
}

//-----------------------------------------------------------------------------
// to convert voice
//-----------------------------------------------------------------------------
export const convertVoice = (voice: voiceDto, user?: string) => {
  const isUpvoted = user
    ? voice.upvotes.some((e) => e.toString() === user.toString())
    : false
  const isDownvoted = user
    ? voice.downvotes.some((e) => e.toString() === user.toString())
    : false
  const isReported = user
    ? voice.reports.some((e) => e.toString() === user.toString())
    : false
  const upvoteCount = voice.upvotes.length
  const downvoteCount = voice.downvotes.length
  const replies = voice.replies || []

  const result: resultVoiceDto = {
    _id: new Types.ObjectId(voice._id),
    owner: `${voice.owner.slice(0, 2)}${voice.owner.slice(-2)}`,
    detail: voice.detail,
    upvotes: upvoteCount,
    downvotes: downvoteCount,
    selfUpvoted: isUpvoted,
    selfDownvoted: isDownvoted,
    selfReported: isReported,
    topics: voice.topics,
    replies: replies
      .map(({ _id, owner, detail }) => ({
        _id: _id,
        owner: `${owner.slice(0, 2)}${owner.slice(-2)}`,
        detail: detail,
        createdTime: new Types.ObjectId(_id).getTimestamp(),
      }))
      .slice(-5),
    replyCount: replies.length,

    createdTime: new Types.ObjectId(voice._id).getTimestamp(),
    reportCount: voice.reportCount,
  }
  return result
}
