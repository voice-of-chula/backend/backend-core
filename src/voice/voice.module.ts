import { Module, forwardRef } from '@nestjs/common'
import { ProposalModule } from 'src/proposal/proposal.module'

import { AuthModule } from 'src/auth/auth.module'
import { RedisModule } from 'src/redis/redis.module'
import { LoggingModule } from 'src/logging/logging.module'
import { VoiceController } from './voice.controller'
import { VoiceService } from './voice.service'
import { VoiceRankingService } from './voiceranking.service'

@Module({
  imports: [
    forwardRef(() => ProposalModule),
    AuthModule,
    RedisModule,
    LoggingModule,
  ],
  controllers: [VoiceController],
  providers: [VoiceService, VoiceRankingService],
  exports: [VoiceService],
})
export class VoiceModule {}
