import {
  CanActivate,
  ExecutionContext,
  Injectable,
  NotFoundException,
} from '@nestjs/common'
import { Observable } from 'rxjs'
import { loggerFromRequest } from 'src/logging/logging.decorator'
import { LogKind } from 'src/logging/logging.service'
import { ProposalService } from '../proposal/proposal.service'

@Injectable()
export class ProposalIdGuard implements CanActivate {
  constructor(private proposalService: ProposalService) {}
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest()
    const logger = loggerFromRequest(request)
    const proposalId = request.params.proposalId

    if (proposalId === 'all') return true

    return this.proposalService
      ._findProposal(proposalId)
      .then(() => {
        return true
      })
      .catch((err) => {
        logger.info(
          { kind: LogKind.INPUTINVALID, proposalId },
          'ProposalGuard Failed',
        )
        throw new NotFoundException('No proposal found')
      })
  }
}
