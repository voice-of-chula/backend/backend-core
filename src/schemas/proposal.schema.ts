import { Schema } from 'mongoose'
import VoiceSchema from './voice.schema'

const ProposalSchema = new Schema({
  _id: String,
  name: { required: true, type: String },
  description: { type: String, default: '' },
  popularVoices: { type: [VoiceSchema], default: [] },
  actions: {
    type: [
      {
        createdAt: Date,
        detail: String,
      },
    ],
    default: [],
  },
})

export default ProposalSchema
