import { Connection, Schema } from 'mongoose'
import { voiceDto } from 'src/voice/voice.dto'
const VoiceSchema = new Schema({
  owner: { required: true, type: String },
  detail: { required: true, type: String },
  reports: { type: [String], default: [] },
  upvotes: { type: [String], default: [] },
  downvotes: { type: [String], default: [] },
  topics: { required: true, type: [String] },
  replies: [{ owner: String, detail: String }],
})

// VoiceSchema.index({ topics: 1, owner: 1 })
VoiceSchema.index({ topics: 1, _id: -1 })

export const VOICE_PREFIX = 'voice-'

export const getVoiceModelFromConnection = (
  connection: Connection,
  proposalId: string,
) =>
  connection.model<voiceDto>(
    VOICE_PREFIX + proposalId,
    VoiceSchema,
    VOICE_PREFIX + proposalId,
  )

export default VoiceSchema
