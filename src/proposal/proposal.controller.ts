import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  Param,
  Post,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common'
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiHeader,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger'
import { AdminGuard } from 'src/auth/admin.guard'
import { JwtPayload } from 'src/auth/auth.dto'
import { User } from 'src/auth/user.decorator'
import { convertVoice } from 'src/voice/voice.service'
import { UserAuthGuard } from 'src/auth/user-auth.guard'
import { ReqLogger } from 'src/logging/logging.decorator'
import Logger from 'bunyan'
import { LogKind } from 'src/logging/logging.service'
import { FunctionalValidatorPipe } from 'src/functional-validator.pipe'
import { ProposalService } from './proposal.service'
import {
  SantitizedProposalDto,
  SantitizedProposalsDto,
  addActionDto,
  createProposalDto,
  getAllProposalsInfo,
  proposalDto,
  proposalDtoResponseLong,
  proposalDtoResponseShort,
} from './proposal.dto'

@Controller('proposal')
@ApiTags('Proposal')
export class ProposalController {
  constructor(
    private readonly proposalService: ProposalService, // private readonly voiceService: VoiceService,
  ) {}

  @Post()
  @UseGuards(AdminGuard)
  @ApiTags('admin')
  @ApiHeader({ name: 'x-admin-token', required: true })
  @ApiOperation({ summary: 'create a new proposal' })
  @ApiCreatedResponse({
    description: 'Create proposal successful',
    type: proposalDto,
  })
  @ApiNotFoundResponse({ description: 'Admin token not found or invalid' })
  async createProposal(
    @Body() data: createProposalDto,
    @ReqLogger() logger: Logger,
  ): Promise<proposalDto> {
    logger.info(
      {
        kind: LogKind.ADMINACTION,
        action: 'create proposal',
        proposalName: data.name,
      },
      'Admin create proposal',
    )
    const proposal = await this.proposalService.createProposal(data)
    return proposal
  }

  @Post(':id/action')
  @UseGuards(AdminGuard)
  @ApiTags('admin')
  @ApiHeader({ name: 'x-admin-token', required: true })
  @ApiOperation({ summary: 'add a new action to a proposal' })
  @ApiCreatedResponse({
    description: 'Add action successful',
    type: proposalDto,
  })
  @ApiNotFoundResponse({ description: 'Admin token not found or invalid' })
  async addAction(
    @Param('id') proposalID: string,
    @Body(
      new FunctionalValidatorPipe(
        (data: addActionDto) => data.detail.length <= 2000,
      ),
    )
    data: addActionDto,
    @ReqLogger() logger: Logger,
  ): Promise<proposalDto> {
    const proposal = await this.proposalService.addAction(proposalID, data)
    logger.info(
      {
        kind: LogKind.ADMINACTION,
        action: 'add action',
        proposalName: proposal.name,
        actionDetail: data.detail,
      },
      'Admin add action',
    )
    return proposal
  }

  // These endpoints is only for development process
  // @Post(':id/upvotevoice')
  // async newUpvoteMock(@Param('id') proposalID: string, @Body() data: voiceDto) {
  //   this.voiceService.checkTop5(data, proposalID)
  //   return data
  // }

  // @Get(':id/serverpopular')
  // async getServerPopVoice(@Param('id') proposalID: string) {
  //   return this.voiceService.getTop5(proposalID)
  // }

  // @Post(':id/popular')
  // async addPopularVoice(
  //   @Param('id') proposalID: string,
  //   @Body() data: voiceDto,
  // ) {
  //   const proposal = await this.proposalService.addPopularVoice(
  //     proposalID,
  //     data,
  //   )
  //   return proposal
  // }

  // @Get(':id/countpopular')
  // async countPopularVoice(@Param('id') proposalID: string) {
  //   const count = await this.proposalService.countPopularVoice(proposalID)
  //   return { count }
  // }

  // @Patch(':id/replacepopular')
  // async replacePopularVoice(
  //   @Param('id') proposalID: string,
  //   @Body() data: voiceDto,
  // ) {
  //   const proposal = await this.proposalService.replaceLeastPopularVoice(
  //     proposalID,
  //     data,
  //   )
  //   return proposal
  // }

  @Get()
  @ApiOperation({ summary: 'Get all proposals' })
  @ApiOkResponse({ type: proposalDtoResponseShort, description: 'OK' })
  @UseInterceptors(ClassSerializerInterceptor)
  async getProposals() {
    const proposals = await this.proposalService.getProposals()
    return proposals.map((p) => new SantitizedProposalDto(p))
  }

  @Get('all')
  @ApiOperation({ summary: 'Get a all proposals' })
  @ApiOkResponse({ type: getAllProposalsInfo, description: 'OK' })
  @ApiBadRequestResponse({ description: 'Could not find proposal' })
  @UseInterceptors(ClassSerializerInterceptor)
  async getAllProposalInfo() {
    const proposal = await this.proposalService.getAllProposalsInfo()
    const ret = new SantitizedProposalsDto({
      count: proposal.count,
      proposals: proposal.proposals,
    })
    return ret
  }

  @Get(':id')
  @ApiOperation({ summary: 'Get a proposal from id' })
  @ApiOkResponse({ type: proposalDtoResponseLong, description: 'OK' })
  @ApiBadRequestResponse({ description: 'Could not find proposal' })
  @UseInterceptors(ClassSerializerInterceptor)
  @UseGuards(UserAuthGuard({ userNullable: true }))
  async getProduct(@Param('id') proposalID: string, @User() user: JwtPayload) {
    const proposal = await this.proposalService.getProposalByID(proposalID)
    return new SantitizedProposalDto({
      ...proposal,
      popularVoices: proposal.popularVoices.map((voice) =>
        convertVoice(voice, (user || {}).ouid),
      ),
    })
  }
}
