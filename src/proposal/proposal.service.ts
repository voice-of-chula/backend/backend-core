import {
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'

import { VoiceService } from '../voice/voice.service'
import {
  addActionDto,
  createProposalDto,
  getAllProposalsInfo,
  proposalDto,
  proposalDtoResponseLong,
  proposalDtoResponseShort,
} from './proposal.dto'

@Injectable()
export class ProposalService {
  constructor(
    @InjectModel('proposals')
    private readonly proposalModel: Model<proposalDto>,
    private readonly voiceService: VoiceService,
  ) { }

  async createProposal(data: createProposalDto): Promise<proposalDto> {
    const proposal = await this.proposalModel.findById(data._id)
    if (proposal) {
      throw new ConflictException('Duplicate proposal _id')
    }
    const newProposal = new this.proposalModel({
      _id: data._id,
      name: data.name,
      description: data.description,
      popularVoices: [],
      actions: [],
    })
    const result = await newProposal.save()
    return result
  }

  async getProposals(): Promise<proposalDtoResponseShort[]> {
    const proposals = await this.proposalModel
      .find({}, { _id: 1, name: 1, description: 1, actions: 1 })
      .exec()
    return proposals
  }

  async getAllProposalsInfo(): Promise<getAllProposalsInfo> {
    const count: number = await this.voiceService.getAllCount()
    const proposals: proposalDtoResponseShort[] = await this.getProposals()
    return { count, proposals }
  }

  // Note: Popular Voices are sorted by popularity
  async getProposalByID(id: string): Promise<proposalDtoResponseLong> {
    const proposal = await this._findProposal(id)

    return {
      _id: proposal._id,
      name: proposal.name,
      description: proposal.description,
      popularVoices: [],
      actions: proposal.actions,
    }
  }

  // // Add a new voice to popularVoice array
  // async addPopularVoice(
  //   proposalID: string,
  //   voice: voiceDto,
  // ): Promise<proposalDto> {
  //   const proposal = await this._findProposal(proposalID)
  //   proposal.popularVoices.push(voice)
  //   const result = await proposal.save()
  //   return result
  // }

  // // Return entire popular voice array
  // async getAllPopularVoice(proposalID: string) {
  //   return (await this._findProposal(proposalID)).popularVoices
  // }

  // // Update popularVoice array with input voices
  // async updateAllPopularVoice(proposalID: string, voices: [voiceDto]) {
  //   const proposal = await this._findProposal(proposalID)
  //   proposal.popularVoices = voices
  //   const result = await proposal.save()
  //   return result
  // }

  // // Return popular voices count (length of the popularVoice array)
  // async countPopularVoice(proposalID: string): Promise<number> {
  //   const proposal = await this._findProposal(proposalID)
  //   return proposal.popularVoices.length
  // }

  // // Replace the voice of the least popular (least upvotes) voice from the popularVoice array with the input
  // // Do nothing if the input voice is less popular than the least of popularVoice array
  // // Update a voice in popularVoice array if the voice has the same _id as the input voice (whether the upvote is greater or less)
  // async replaceLeastPopularVoice(
  //   proposalID: string,
  //   voice: voiceDto,
  // ): Promise<proposalDto> {
  //   const proposal = await this._findProposal(proposalID)
  //   const voices = proposal.popularVoices

  //   // Checking if the input voice is already in the popularVoice array (If yes, replace then)
  //   for (let i = 0; i < voices.length; i++) {
  //     // For some reason voices[i]._id actually has type of object (vscode sees it as string though), not sure about voice._id
  //     if (voices[i]._id.toString() === voice._id.toString()) {
  //       proposal.popularVoices[i] = voice
  //       const result = await proposal.save()
  //       return result
  //     }
  //   }

  //   // Finding the least popular voice
  //   let minVoiceInd = 0,
  //     minVoiceScore = this.voiceService.getVoiceScore(voices[0])
  //   for (let i = 0; i < voices.length; i++) {
  //     if (this.voiceService.getVoiceScore(voices[i]) < minVoiceScore) {
  //       minVoiceScore = this.voiceService.getVoiceScore(voices[i])
  //       minVoiceInd = i
  //     }
  //   }

  //   // Replacing the least popular voice
  //   // Assumption: if the input voice is as popular as the leastPopularVoice, the leastPoupularVoice will be replaced anyways
  //   if (minVoiceScore > this.voiceService.getVoiceScore(voice)) {
  //     return proposal
  //   }
  //   proposal.popularVoices[minVoiceInd] = voice
  //   const result = await proposal.save()
  //   return result
  // }

  // Add a new action to actions array
  async addAction(
    proposalID: string,
    action: addActionDto,
  ): Promise<proposalDto> {
    const proposal = await this._findProposal(proposalID)
    proposal.actions.push(action)
    const result = await proposal.save()
    return result
  }

  async _findProposal(id: string): Promise<proposalDto> {
    try {
      const proposal = await this.proposalModel.findById(id)
      if (!proposal) {
        throw new NotFoundException('Could not find proposal')
      }

      return proposal
    } catch (error) {
      throw new NotFoundException('Could not find proposal')
    }
  }
}
