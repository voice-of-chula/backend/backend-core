import { Module, forwardRef } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'

import { AuthModule } from 'src/auth/auth.module'
import ProposalSchema from '../schemas/proposal.schema'
import { VoiceModule } from '../voice/voice.module'
import { ProposalController } from './proposal.controller'
import { ProposalService } from './proposal.service'

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'proposals', schema: ProposalSchema }]),
    forwardRef(() => VoiceModule),
    AuthModule,
  ],
  controllers: [ProposalController],
  providers: [ProposalService],
  exports: [ProposalService],
})
export class ProposalModule {}
