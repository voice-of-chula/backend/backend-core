import { ApiProperty } from '@nestjs/swagger'
import { Transform, Type } from 'class-transformer'
import { IsDate, IsString } from 'class-validator'
import { Document } from 'mongoose'
import * as _sanitizeHtml from 'sanitize-html'
import { voiceDto } from '../voice/voice.dto'

const { defaults } = _sanitizeHtml

const sanitizeHtml = (value) =>
  _sanitizeHtml(value, {
    ...defaults,
    allowedAttributes: {
      ...defaults.allowedAttributes,
      p: ['style'],
    },
  })

export class proposalDto extends Document {
  @ApiProperty()
  _id: string

  @ApiProperty()
  name: string

  @ApiProperty()
  description: string

  @ApiProperty()
  popularVoices: voiceDto[]

  @ApiProperty()
  actions: [
    {
      createdAt: Date
      detail: string
    },
  ]
}

export class proposalDtoResponseShort {
  @ApiProperty()
  _id: string

  @ApiProperty()
  name: string

  @ApiProperty()
  description: string

  @ApiProperty()
  actions: {
    createdAt: Date
    detail: string
  }[]
}

export class proposalDtoResponseLong {
  @ApiProperty()
  _id: string

  @ApiProperty()
  name: string

  @ApiProperty()
  description: string

  @ApiProperty()
  popularVoices: voiceDto[]

  @ApiProperty()
  actions: [
    {
      createdAt: Date
      detail: string
    },
  ]
}

export class getAllProposalsInfo {
  @ApiProperty()
  count: number

  @ApiProperty()
  proposals: proposalDtoResponseShort[]
}

export class createProposalDto {
  @ApiProperty()
  @IsString()
  _id: string

  @ApiProperty()
  @IsString()
  name: string

  @ApiProperty()
  @IsString()
  description: string
}

export class addActionDto {
  @ApiProperty()
  @Type(() => Date)
  @IsDate()
  createdAt: Date

  @ApiProperty()
  @IsString()
  detail: string
}

export class SantitizedProposalDto {
  @ApiProperty()
  @Transform(sanitizeHtml)
  description: string

  constructor(obj: SantitizedProposalDto & any) {
    Object.assign(this, JSON.parse(JSON.stringify(obj)))
  }
}

export class SantitizedProposalsDto {
  @ApiProperty()
  count: number

  @ApiProperty()
  proposals: SantitizedProposalDto[]

  constructor(obj: Partial<SantitizedProposalsDto>) {
    this.count = obj.count
    this.proposals = JSON.parse(JSON.stringify(obj.proposals)).map(
      (proposal) => new SantitizedProposalDto(proposal),
    )
  }
}
