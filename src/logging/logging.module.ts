import { Module } from '@nestjs/common'
import { CustomLogger } from './customlogger.service'
import { LoggingService, loggerFactory } from './logging.service'

@Module({
  providers: [
    {
      provide: LoggingService,
      useFactory: loggerFactory,
    },
    CustomLogger,
  ],
  exports: [CustomLogger, LoggingService],
})
export class LoggingModule {}
