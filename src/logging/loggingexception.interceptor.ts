import {
  CallHandler,
  ExecutionContext,
  HttpException,
  HttpStatus,
  Injectable,
  NestInterceptor,
} from '@nestjs/common'
import { Observable } from 'rxjs'
import { tap } from 'rxjs/operators'
import { loggerFromRequest } from './logging.decorator'
import { LogKind } from './logging.service'

@Injectable()
export class LoggingExceptionInterceptor implements NestInterceptor {
  intercept(
    context: ExecutionContext,
    next: CallHandler<any>,
  ): Observable<any> | Promise<Observable<any>> {
    return next.handle().pipe(
      tap({
        error: (err) => {
          const req = context.switchToHttp().getRequest()
          const logger = loggerFromRequest(req)
          const status =
            err instanceof HttpException
              ? err.getStatus()
              : HttpStatus.INTERNAL_SERVER_ERROR
          if (status < 500) {
            logger.info({
              err,
              source: 'Exception Logging Interceptor',
              handler: context.getHandler().name,
              kind: LogKind.INPUTINVALID,
              requestBody: req.body,
            })
          } else {
            logger.error({
              err,
              source: 'Exception Logging Interceptor',
              handler: context.getHandler().name,
              kind: LogKind.APPERROR,
              requestBody: req.body,
            })
          }
        },
      }),
    )
  }
}
