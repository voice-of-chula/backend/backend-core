import { ExecutionContext, createParamDecorator } from '@nestjs/common'
import * as Logger from 'bunyan'

export const ReqLogger = createParamDecorator(
  (data: any, context: ExecutionContext): Logger =>
    loggerFromRequest(context.switchToHttp().getRequest()),
)

export const loggerFromRequest = (req: any): Logger => {
  let logger: Logger = req.log
  if (req.user) logger = logger.child({ user: req.user })
  return logger
}
