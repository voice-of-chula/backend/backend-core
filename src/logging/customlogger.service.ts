import { Injectable, Logger, Scope } from '@nestjs/common'
import { LoggingService } from './logging.service'

/**
 * Replacement for NestJS's global logging
 */
@Injectable({ scope: Scope.TRANSIENT })
export class CustomLogger extends Logger {
  context: string

  constructor(readonly logger: LoggingService) {
    super()
  }

  error(message: any, trace?: string, context?: string) {
    this.logger
      .getLogger()
      .error({ message, trace, context: context || this.context })
  }

  log(message: any, context?: string) {
    this.logger.getLogger().info({ message, context: context || this.context })
  }

  warn(message: any, context?: string) {
    this.logger.getLogger().warn({ message, context: context || this.context })
  }

  setContext(context: string) {
    this.context = context
  }
}
