import * as Logger from 'bunyan'
import * as Glb from '@google-cloud/logging-bunyan'
import { NextFunction, Response } from 'express'

export class LoggingService {
  constructor(private logger: Logger, private middleware: any) {}

  getMiddleware() {
    return this.middleware
  }

  getLogger(): Logger {
    return this.logger
  }
}

export const loggerFactory = async () => {
  if (process.env.GAE_DEPLOYMENT_ID) {
    //We are on GAE
    const { logger, mw } = await Glb.express.middleware({
      logName: 'VoC-Backend',
    })
    return new LoggingService(logger, mw)
  } else {
    const logger = Logger.createLogger({
      name: 'Voc Backend Local',
      serializers: {
        req: Logger.stdSerializers.req,
        err: Logger.stdSerializers.err,
      },
    })
    logger.info({ message: 'Local logging activated' })
    const mw = (req: any, res: Response, next: NextFunction) => {
      req.log = logger.child({
        req,
      })
      next()
    }

    return new LoggingService(logger, mw)
  }
}

export enum LogKind {
  APPERROR = 'apperror',
  AUTHINFO = 'authinfo',
  INPUTINVALID = 'inputinvalid',
  ADMINACTION = 'adminaction',
  USERCONTENT = 'usercontent',
}
