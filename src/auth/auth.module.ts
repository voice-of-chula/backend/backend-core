import { HttpModule, Module } from '@nestjs/common'
import { JwtModule } from '@nestjs/jwt'
import { ConfigService } from '@nestjs/config'
import { MongooseModule } from '@nestjs/mongoose'
import { AuthService } from './auth.service'
import { AuthController } from './auth.controller'
import { JwtStrategy } from './jwt.strategy'
import { User, UserSchema } from './auth.model'

@Module({
  providers: [AuthService, JwtStrategy],
  controllers: [AuthController],
  imports: [
    HttpModule,
    JwtModule.registerAsync({
      useFactory: (config: ConfigService) => ({
        secret: jwtSecretFromConfig(config),
      }),
      inject: [ConfigService],
    }),
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
  ],
  exports: [JwtModule],
})
export class AuthModule {}

export const jwtSecretFromConfig = (config: ConfigService) =>
  config.get<string>('googlesecrets.jwt') || config.get<string>('JWT_SECRET')
