import { ApiProperty } from '@nestjs/swagger'
import { IsString } from 'class-validator'

type SSORole = 'faculty' | 'student'

export class SSOValidationResponse {
  uid: string
  username: string
  gecos: string
  disable: boolean
  roles: SSORole[]
  firstname: string
  firstnameth: string
  lastname: string
  lastnameth: string
  ouid: string
  email: string
}

export class JwtPayload {
  ouid: string
}

export class ExchangeTicketDTO {
  @ApiProperty({ description: 'Ticket from Chula SSO' })
  @IsString()
  ticket: string
}

export class WebappTokensDTO {
  @ApiProperty()
  accessToken: string
}
