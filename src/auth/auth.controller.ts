import {
  Body,
  Controller,
  Get,
  HttpException,
  Post,
  Req,
  Res,
  UnauthorizedException,
  UseGuards,
} from '@nestjs/common'
import {
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiInternalServerErrorResponse,
  ApiOkResponse,
  ApiSecurity,
  ApiTags,
  ApiUnauthorizedResponse,
  ApiUnprocessableEntityResponse,
} from '@nestjs/swagger'
import { CookieOptions, Request, Response } from 'express'
import { ConfigService } from '@nestjs/config'
import Logger from 'bunyan'
import { ReqLogger } from 'src/logging/logging.decorator'
import { LogKind } from 'src/logging/logging.service'
import { User } from './user.decorator'
import { ExchangeTicketDTO, JwtPayload, WebappTokensDTO } from './auth.dto'
import { AuthService, UserId } from './auth.service'
import { UserAuthGuard } from './user-auth.guard'

const REFRESH_TOKEN_COOKIE_NAME = 'voice_of_chula_refresh_token'

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(
    private authService: AuthService,
    private configService: ConfigService,
  ) {}

  @Post('exchangeticket')
  @ApiCreatedResponse({
    type: WebappTokensDTO,
  })
  @ApiInternalServerErrorResponse({
    description: 'Connection to Chula SSO failed',
  })
  @ApiUnprocessableEntityResponse({
    description: 'Ticket failed SSO verification',
  })
  @ApiForbiddenResponse({
    description: 'User is not a freshman',
  })
  async exchangeTicket(
    @Body() ticketDTO: ExchangeTicketDTO,
    @Res() res: Response,
    @ReqLogger() logger: Logger,
  ) {
    const user = await this.authService.exchangeTicketToUser(
      ticketDTO.ticket,
      logger,
    )

    const {
      accessToken,
      refreshToken,
    } = await this.authService.generateTokensForUser(user, logger)

    this.setRefreshCookie(res, refreshToken)
    res.json({ accessToken } as WebappTokensDTO)
  }

  private getRefreshCookieOpt(): CookieOptions {
    const opt = {
      httpOnly: true,
    } as CookieOptions
    if (this.configService.get<string>('SECURE_COOKIE') !== 'no') {
      opt.secure = true
      opt.sameSite = 'none'
    }
    return opt
  }

  private setRefreshCookie(res: Response, refreshToken: string) {
    res.cookie(
      REFRESH_TOKEN_COOKIE_NAME,
      refreshToken,
      this.getRefreshCookieOpt(),
    )
  }

  private async getUserFromRefreshCookie(@Req() req: Request): Promise<UserId> {
    const token = req.cookies[REFRESH_TOKEN_COOKIE_NAME]
    if (!token) throw new UnauthorizedException('No refresh cookie found')
    const user = await this.authService.userFromRefreshToken(token)
    if (!user) throw new UnauthorizedException('Refresh token revoked')
    return user
  }

  @Post('refresh_token')
  @ApiCreatedResponse({ type: WebappTokensDTO })
  @ApiUnauthorizedResponse({
    description: 'Refresh token expired or no refresh token',
  })
  async refreshToken(
    @Res() res: Response,
    @Req() req: Request,
    @ReqLogger() logger: Logger,
  ) {
    const user = await this.getUserFromRefreshCookie(req)
    const {
      accessToken,
      refreshToken,
    } = await this.authService.generateTokensForUser(user, logger)
    this.setRefreshCookie(res, refreshToken)
    res.json({ accessToken } as WebappTokensDTO)
    logger.info({ kind: LogKind.AUTHINFO }, 'Re-issued tokens')
  }

  @Post('logout')
  @ApiCreatedResponse({ description: 'RefreshToken revoked' })
  async clearRefreshToken(
    @Res() res: Response,
    @Req() req: Request,
    @ReqLogger() logger: Logger,
  ) {
    try {
      const user = await this.getUserFromRefreshCookie(req)
      await this.authService.generateTokensForUser(user, logger)
      logger.info(
        {
          kind: LogKind.AUTHINFO,
        },
        'Revoked tokens, user logged out',
      )
    } catch (e) {
      if (e instanceof HttpException) {
        //Nothing
      } else {
        throw e
      }
    }

    res.clearCookie(REFRESH_TOKEN_COOKIE_NAME, this.getRefreshCookieOpt())
    res.status(201).send('Refresh Token revoked')
  }

  @Get('me')
  @ApiOkResponse({ description: 'Current authenticated user' })
  @ApiSecurity('access token')
  @UseGuards(UserAuthGuard({ userNullable: true }))
  getAuthenticatedUser(@User() user: JwtPayload) {
    return {
      user,
    }
  }
}
