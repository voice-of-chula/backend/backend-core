import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document } from 'mongoose'

@Schema()
export class User extends Document {
  @Prop({ required: true, index: true, unique: true })
  ouid: string

  @Prop({ required: true, index: true, unique: true })
  refreshToken: string
}

export const UserSchema = SchemaFactory.createForClass(User)
