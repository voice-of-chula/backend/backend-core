import {
  CanActivate,
  ExecutionContext,
  Injectable,
  NotFoundException,
} from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { loggerFromRequest } from 'src/logging/logging.decorator'
import { LogKind } from 'src/logging/logging.service'

@Injectable()
export class AdminGuard implements CanActivate {
  constructor(private configService: ConfigService) {}
  canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest()
    const token = request.headers['x-admin-token']
    const { method, url } = request
    const logger = loggerFromRequest(request)

    if (
      token ==
      (this.configService.get<string>('googlesecrets.admin_token') ||
        this.configService.get<string>('__ADMIN_TOKEN'))
    ) {
      return true
    }

    logger.info(
      { kind: LogKind.INPUTINVALID },
      'Admin token validation fail for AdminGuard',
    )
    throw new NotFoundException(
      `Cannot ${method} ${url}. Requires valid admin token.`,
    )
  }
}
