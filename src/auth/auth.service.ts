import {
  HttpService,
  Injectable,
  InternalServerErrorException,
  UnprocessableEntityException,
} from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { classToPlain, plainToClass } from 'class-transformer'
import { JwtService } from '@nestjs/jwt'
import Axios from 'axios'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'
import { v4 as uuid4 } from 'uuid'
import Logger from 'bunyan'
import { LogKind } from 'src/logging/logging.service'
import { User } from './auth.model'
import { JwtPayload, SSOValidationResponse } from './auth.dto'

@Injectable()
export class AuthService {
  constructor(
    private httpService: HttpService,
    private configService: ConfigService,
    private jwtService: JwtService,
    @InjectModel(User.name)
    private userModel: Model<User>,
  ) {}

  async validateChulaSSOTicket(
    ticket: string,
    auditLog: Logger,
  ): Promise<SSOValidationResponse> {
    try {
      const source = Axios.CancelToken.source()
      const response_promise = this.httpService
        .get(
          this.configService.get<string>('CHULASSO_URL') + '/serviceValidation',
          {
            headers: {
              DeeAppId: this.configService.get<string>('CHULASSO_APPID'),
              DeeAppSecret:
                this.configService.get<string>('googlesecrets.ssosecret') ||
                this.configService.get<string>('CHULASSO_APPSECRET'),
              DeeTicket: ticket,
            },
            cancelToken: source.token,
          },
        )
        .toPromise()

      let resolved = false

      setTimeout(() => {
        if (!resolved) {
          source.cancel('Time limit exceeded')
        }
      }, 5000)

      const response = await response_promise
      resolved = true

      return plainToClass(SSOValidationResponse, response.data)
    } catch (error) {
      if (error.response && error.response.status == 401) {
        const message = error.response.data.content
        auditLog.info(
          {
            ssoResponse: {
              status: error.response?.status,
              data: error.response?.data,
            },
            kind: LogKind.AUTHINFO,
          },
          'Validating ticket with SSO failed, ticket invalid',
        )
        throw new UnprocessableEntityException(
          'SSO ticket verification failed: ' + message,
        )
      } else if (Axios.isCancel(error)) {
        auditLog.error(
          {
            kind: LogKind.APPERROR,
          },
          'Validating ticket with SSO failed, SSO took too long to response',
        )
        throw new InternalServerErrorException(
          'Took to long to connect to Chula SSO',
        )
      } else {
        auditLog.error(
          {
            kind: LogKind.APPERROR,
            err: error,
          },
          'SSO Validation fail. Unknown error',
        )
        throw new InternalServerErrorException("Can't connect to Chula SSO")
      }
    }
  }

  async exchangeTicketToUser(
    ssoTicket: string,
    auditLog: Logger,
  ): Promise<UserId> {
    auditLog.info(
      {
        kind: LogKind.AUTHINFO,
      },
      'Authenticating reqeust with SSO',
    )
    const ssoData = await this.validateChulaSSOTicket(ssoTicket, auditLog)
    auditLog.info(
      {
        ssoId: ssoData.ouid,
        kind: LogKind.AUTHINFO,
      },
      'Authenticated with SSO',
    )
    return ssoData.ouid
  }

  //Also has side effect of invalidating refresh token
  async generateTokensForUser(
    user: UserId,
    auditLog: Logger,
  ): Promise<InternalTokenDTO> {
    const jwtToken = this.signJwt({
      ouid: user,
    })

    //Invalidate refresh token
    const refreshToken: string = uuid4()
    await this.userModel.findOneAndUpdate(
      { ouid: user },
      { refreshToken: refreshToken },
      { upsert: true },
    )

    auditLog.info(
      {
        kind: LogKind.AUTHINFO,
        userId: user,
      },
      'Reset refresh token',
    )

    return {
      accessToken: jwtToken,
      refreshToken: refreshToken,
    }
  }

  async userFromRefreshToken(refreshToken: string): Promise<UserId | null> {
    const user = await this.userModel.findOne({ refreshToken })
    if (user) {
      return user.ouid
    } else {
      return null
    }
  }

  signJwt(payload: JwtPayload): string {
    return this.jwtService.sign(classToPlain(payload), { expiresIn: '1h' })
  }
}

export type UserId = string
export interface InternalTokenDTO {
  accessToken: string
  refreshToken: string
}
