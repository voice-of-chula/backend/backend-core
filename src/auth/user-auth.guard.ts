import { ExecutionContext, mixin } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { loggerFromRequest } from 'src/logging/logging.decorator'
import { LogKind } from 'src/logging/logging.service'

/**
 * A guard that initialize req.user.
 * If the user is authenticated, req.user will be set to JwtPayload of the user.
 * Otherwise, an unauthorized exception is thrown. (except when suppressed with userNullable flag,
 * in that case, req.user will be null)
 * @param failOnAuthFailure Should the guard fail if user is not authenticated?
 * @returns
 */
export const UserAuthGuard = (opt?: { userNullable?: boolean }) => {
  const userNullable = opt?.userNullable || false

  class MixinJwtAuthGuard extends AuthGuard('jwt') {
    async canActivate(context: ExecutionContext): Promise<boolean> {
      const req = context.switchToHttp().getRequest()
      const logger = loggerFromRequest(req)
      req.user = null
      let result = false
      try {
        result = await (super.canActivate(context) as Promise<boolean>)
      } catch (e) {
        if (!userNullable) {
          logger.info(
            { kind: LogKind.AUTHINFO },
            'Endpoint authentication check fail. No user authenticated',
          )
          throw e
        }
      }
      return result || userNullable
    }
  }

  return mixin(MixinJwtAuthGuard)
}
