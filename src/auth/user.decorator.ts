import { ExecutionContext, createParamDecorator } from '@nestjs/common'

/**
 * Decorate controller's parameter to retrieve user's JWT Payload'
 * Must use along with UserAuthGuard
 */
export const User = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    return ctx.switchToHttp().getRequest().user
  },
)
