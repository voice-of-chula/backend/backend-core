import { Module } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { RedisClient, createClient } from 'redis'
import { LoggingModule } from 'src/logging/logging.module'
import { LoggingService } from 'src/logging/logging.service'

@Module({
  providers: [
    {
      provide: RedisClient,
      useFactory: (configService: ConfigService, logging: LoggingService) => {
        const client = createClient({
          db: configService.get<string>('REDIS_DB'),
          password:
            configService.get<string>('REDIS_PASSWORD') ||
            configService.get<string>('googlesecrets.redispassword'),
          host: configService.get<string>('REDIS_HOST'),
          socket_initial_delay: 60000,
        })
        client.on('error', (err) => {
          logging.getLogger().error({ msg: 'Error in Redis Connection', err })
        })
        client.on('ready', () => {
          logging.getLogger().info('Redis ready')
        })
        return client
      },
      inject: [ConfigService, LoggingService],
    },
  ],
  imports: [LoggingModule],
  exports: [RedisClient],
})
export class RedisModule {}
