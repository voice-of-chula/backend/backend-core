import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from '@nestjs/common'

/**
 * A pipe that validate the value given using the specified function.
 * It will pass-on the value iff any of this condition is true
 *    - the specified function return true
 *    - the value is undefined and isOptional flag is set
 * Otherwise it will throw BadRequest error.
 */
@Injectable()
export class FunctionalValidatorPipe implements PipeTransform {
  constructor(
    private fn: (arg: any) => boolean,
    private isOptional: boolean = false,
  ) {}

  transform(value: any, metadata: ArgumentMetadata) {
    if (value === undefined && this.isOptional) {
      return undefined
    }
    if (!this.fn(value))
      throw new BadRequestException(
        `argument ${JSON.stringify(value)} for ${
          metadata.data || metadata.type
        } is an incorrectly formatted`,
      )
    return value
  }
}
