import { Injectable } from '@nestjs/common'
import { VoiceService } from '../voice/voice.service'

@Injectable()
export class TopicService {
  constructor(private voiceService: VoiceService) {}

  async getAllTopic(proposalId: string) {
    const topics = await this.voiceService.getAllTopic(proposalId)

    const result = []

    for (const topic in topics) {
      const count = await this.voiceService.countVoiceByTopic(
        topics[topic],
        proposalId,
      )

      result.push({ name: topics[topic], count: count[0]['count'] })
    }
    return result
  }
}
