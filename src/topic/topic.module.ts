import { Module } from '@nestjs/common'
import { VoiceModule } from '../voice/voice.module'
import { TopicController } from './topic.controller'
import { TopicService } from './topic.service'

@Module({
  imports: [VoiceModule],
  controllers: [TopicController],
  providers: [TopicService],
  exports: [TopicService],
})
export class TopicModule {}
