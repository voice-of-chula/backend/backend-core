import { Document } from 'mongoose'
import { ApiProperty } from '@nestjs/swagger'

export class getTopicDto extends Document {
  @ApiProperty()
  name: string

  @ApiProperty()
  count: number
}
