import { Controller, Get, Param } from '@nestjs/common'
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger'
import { getTopicDto } from './topic.dto'
import { TopicService } from './topic.service'

@Controller('proposal/:proposalId')
@ApiTags('topic')
export class TopicController {
  constructor(private topicService: TopicService) {}
  @Get('topic')
  @ApiOperation({
    summary: 'get all topics from a proposal ',
  })
  @ApiOkResponse({ type: [getTopicDto], description: 'OK' })
  async getTopic(@Param('proposalId') proposalId) {
    return await this.topicService.getAllTopic(proposalId)
  }
}
