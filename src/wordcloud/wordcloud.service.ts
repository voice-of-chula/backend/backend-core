import { promises as fs } from 'fs'
import { Injectable } from '@nestjs/common'
import { proposalDtoResponseLong } from 'src/proposal/proposal.dto'
import { ProposalService } from 'src/proposal/proposal.service'
import { TopicService } from 'src/topic/topic.service'
import * as puppeteer from 'puppeteer'

let li = []
const Mp = new Map()
const MAX_FONT_SIZE = 100
const MIN_FONT_SIZE = 50

async function htmlGen(proposalInfo: proposalDtoResponseLong) {
  const a = `<!DOCTYPE html>
    <html>
    <script src="d3.js"></script>
    <script src="d3.layout.cloud.js"></script>
    <style>
        @font-face {
          font-family: 'ChulaCharasNew';
          src: url('ChulaCharasNewReg.ttf');
          font-weight: normal;
          font-style: normal;
        }

        @font-face {
          font-family: 'CHULALONGKORN';
          src: url('CHULALONGKORNReg.otf');
          font-weight: normal;
          font-style: normal;
        }


        body {
            font-family:"Lucida Grande","Droid Sans",Arial,Helvetica,sans-serif;
            background-color: #F8E1EA;
        }
        .legend {
            border: 1px solid #555555;
            border-radius: 5px 5px 5px 5px;
            font-size: 0.8em;
            margin: 10px;
            padding: 8px;
        }
        .bld {
            font-weight: bold;
        }
    </style>
    <body>
    <div style="height: 60px;display: flex;align-items: center;">
      <svg id="Layer_2" data-name="Layer 2" style="height: 40px;margin-bottom: 10px;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1380.77 1380.77"><defs><style>.cls-1,.cls-2{fill:none;stroke:#de5c8e;stroke-linecap:round;stroke-miterlimit:10;}.cls-1{stroke-width:80px;}.cls-2{stroke-width:50px;}</style></defs><path class="cls-1" d="M167.25,1215.73c-118.09-118.09,14.14-216.38,85.56-170.42l156.9-646.37S527.88,333.26,788.8,594.18"/><circle class="cls-1" cx="882.31" cy="500.67" r="51.25"/><line class="cls-2" x1="1189.73" y1="193.25" x2="990.32" y2="392.66"/><line class="cls-2" x1="893.45" y1="202.44" x2="893.45" y2="343.86"/><line class="cls-2" x1="575.25" y1="233.56" x2="768.29" y2="426.6"/><path class="cls-1" d="M167.25,1215.73c118.09,118.08,216.38-14.14,170.41-85.56L984,973.27S1049.72,855.1,788.8,594.18"/><circle class="cls-1" cx="882.31" cy="500.67" r="51.25"/><line class="cls-2" x1="1189.73" y1="193.25" x2="990.32" y2="392.66"/><line class="cls-2" x1="1180.54" y1="489.53" x2="1039.11" y2="489.53"/><line class="cls-2" x1="1149.42" y1="807.73" x2="956.38" y2="614.69"/></svg>
      <strong style="font-family: ChulaCharasNew; font-size: 30px; text-align: left; color: #DE5C8E; margin-left: 5px">
      Voice of Chula
      </strong>
    </div>
    </body>
    <script>

        var frequency_list = ${(function () {
          let str = '['
          for (let i = 0; i < li.length; i++) {
            str += `{"text" : "${li[i].text}", "size" : ${li[i].size}}`
            if (i != li.length - 1) str += ','
          }
          str += ']'
          return str
        })()}


        var possibleDegree = [0, 90]

        var color = d3.scale.linear()
                .domain([0,1,2,3,4,5,6,10,15,20,100])
                .range(["#ccc", "#bbb", "#aaa", "#999", "#888", "#777", "#666", "#555", "#444", "#333", "#222"]);

        d3.layout.cloud().size([1200, 480])
          .words(frequency_list)
          .fontSize(function(d) { return d.size; })
          .rotate(0)
          .spiral("archimedean")
          .on("end", draw)
          .start();

          function draw(words) {

            d3.select("body").append("svg")
                .attr("width", 1200)
                .attr("height", 480)
                .attr("class", "wordcloud")
                .append("g")
                // without the transform, words words would get cutoff to the left and top, they would
                // appear outside of the SVG area
                .attr("transform", "translate(500 , 300)")
                .selectAll("text")
                .data(words)
                .enter().append("text")
                .style("font-size", function(d) { return d.size + "px"; })
                .style("font-family", "ChulaCharasNew")
                .style("fill", function(d, i) { return color(i); })
                .attr("transform", function(d) {
                  return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                })
                .text(function(d) { return d.text; });
                
            d3.select("body").append("h1")
                .style("font-family", "CHULALONGKORN")
                .style("font-size", "30px")
                .style("text-align", "center")
                .style("background-color", "#DE5C8E")
                .style("color", "white")
                .text("${proposalInfo.name}")

        }
    </script>
    </html>`
  await fs
    .writeFile(`/tmp/forRender_${proposalInfo._id}.html`, a, 'utf-8')
    .then(() => {})
    .catch((er) => {
      console.log(er)
    })
  return
}

@Injectable()
export class WordcloudService {
  constructor(
    private topicService: TopicService,
    private proposalService: ProposalService,
  ) {}
  async updateList(proposalId: string): Promise<void> {
    li = []
    const topics = await this.topicService.getAllTopic(proposalId)
    if (topics.length == 0) {
      return
    }
    topics.sort((a, b) => {
      return a.count > b.count ? -1 : 1
    })

    const maxCount = topics[0].count
    for (let i = 0; i < Math.min(30, topics.length); i++) {
      li.push({
        text: topics[i].name,
        size:
          MIN_FONT_SIZE +
          (topics[i].count * (MAX_FONT_SIZE - MIN_FONT_SIZE)) / maxCount,
      })
    }
    return
  }
  async update(proposalId: string, timeStamp: number) {
    if (Mp.has(proposalId) && timeStamp - Mp.get(proposalId) <= 5000) {
      return
    } else {
      Mp.set(proposalId, timeStamp)
      await this.updateList(proposalId)
      await htmlGen(await this.proposalService.getProposalByID(proposalId))
      // const url = `file://${__dirname}/../../src/wordcloud/forRender.html`
      const url = `file:///tmp/forRender_${proposalId}.html`
      async function run() {
        const browser = await puppeteer.launch({
          headless: true,
          args: ['--no-sandbox', '--disable-setuid-sandbox'],
        })
        const page = await browser.newPage()
        await page.goto(url, { waitUntil: 'networkidle0', timeout: 60000 })
        await page.setViewport({ width: 1200, height: 630 })
        await page.screenshot({
          path: '/tmp/' + proposalId + '.jpg',
          type: 'jpeg',
        })
        await page.close()
        await browser.close()
      }
      await run()
    }
  }
}
