import { Module } from '@nestjs/common'
import { ProposalModule } from 'src/proposal/proposal.module'
import { TopicModule } from 'src/topic/topic.module'
import { WordcloudController } from './wordcloud.controller'
import { WordcloudService } from './wordcloud.service'

@Module({
  imports: [TopicModule, ProposalModule],
  controllers: [WordcloudController],
  providers: [WordcloudService],
})
export class WordcloudModule {}
