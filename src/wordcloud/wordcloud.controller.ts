import { Controller, Get, Param, Res } from '@nestjs/common'
import { Response } from 'express'
import { WordcloudService } from './wordcloud.service'
@Controller('wordcloud/:proposalId/:timeStamp')
export class WordcloudController {
  constructor(private wordcloudService: WordcloudService) {}
  @Get('')
  async getPhoto(
    @Param('proposalId') proposalId: string,
    @Param('timeStamp') timeStamp: number,
    @Res() res: Response,
  ) {
    this.wordcloudService.update(proposalId, timeStamp).then(() => {
      res.sendFile(proposalId + '.jpg', { root: '/tmp/' })
    })
  }
}
