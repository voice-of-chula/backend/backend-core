import { SecretManagerServiceClient } from '@google-cloud/secret-manager'

export default async () => {
  const variablesConfig = process.env.GOOGLESECRET

  if (!variablesConfig) return {}

  const mapping = variablesConfig.split(':').map((s) => s.split('<>'))

  const client = new SecretManagerServiceClient()

  const result: Record<string, string> = {}

  for (const [key, path] of mapping) {
    const [version] = await client.accessSecretVersion({ name: path })
    result[key] = version.payload.data.toString()
  }

  return { googlesecrets: result }
}
