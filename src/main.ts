import { NestFactory } from '@nestjs/core'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'
import * as cookieParser from 'cookie-parser'
import * as cors from 'cors'
import { ConfigService } from '@nestjs/config'
import { NestExpressApplication } from '@nestjs/platform-express'
import { AppModule } from './app.module'
import { CustomLogger } from './logging/customlogger.service'
import { LoggingService } from './logging/logging.service'

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule)

  app.set('trust proxy', true)

  const logging = app.get<LoggingService>(LoggingService)
  app.useLogger(new CustomLogger(logging))
  app.use(logging.getMiddleware())

  app.setGlobalPrefix('api')

  const swaggerOptions = new DocumentBuilder()
    .setTitle('Voice of Chula API')
    .setVersion('1.0')
    .setExternalDoc(
      'Chula SSO documentation',
      'https://account.it.chula.ac.th/wiki/doku.php?id=how_does_it_work',
    )
    .addBearerAuth({ type: 'http', bearerFormat: 'JWT' }, 'access token')
    .build()
  const document = SwaggerModule.createDocument(app, swaggerOptions)
  SwaggerModule.setup('/api/swagger', app, document)

  app.use(cookieParser())

  const port =
    parseInt(app.get<ConfigService>(ConfigService).get<string>('PORT')) || 3000

  app.use(
    cors({
      origin: [
        'http://localhost:9000',
        'https://voice-of-chula.df.r.appspot.com',
        'https://frontend-staging-dot-voice-of-chula.df.r.appspot.com',
        'https://voice-of-chula.online',
        'https://www.voice-of-chula.online',
      ],
      credentials: true,
    }),
  )

  await app.listen(port)
}
bootstrap()
