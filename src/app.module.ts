import { Module, ValidationPipe } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core'
import { MongooseModule } from '@nestjs/mongoose'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { AuthModule } from './auth/auth.module'
import { ProposalModule } from './proposal/proposal.module'
import { TopicModule } from './topic/topic.module'
import { VoiceModule } from './voice/voice.module'
import { WordcloudModule } from './wordcloud/wordcloud.module'
import { LoggingModule } from './logging/logging.module'
import googleSecret from './google-secret'
import { LoggingExceptionInterceptor } from './logging/loggingexception.interceptor'
import { RedisModule } from './redis/redis.module'

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: ['.env.local', '.env'],
      load: [googleSecret],
    }),
    MongooseModule.forRootAsync({
      useFactory: async (configService: ConfigService) => {
        const user = configService.get<string>('MONGODB_USER')
        const pass =
          configService.get<string>('googlesecrets.mongodb_pass') ||
          configService.get<string>('MONGODB_PASS')
        return {
          uri: configService.get<string>('MONGODB_URI'),
          auth:
            user && pass
              ? {
                  user: user,
                  password: pass,
                }
              : null,
          authSource: 'admin',
        }
      },
      inject: [ConfigService],
    }),
    VoiceModule,
    ProposalModule,
    AuthModule,
    TopicModule,
    WordcloudModule,
    LoggingModule,
    RedisModule,
  ],

  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_PIPE,
      useFactory: () =>
        new ValidationPipe({
          whitelist: true,
          forbidNonWhitelisted: true,
        }),
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggingExceptionInterceptor,
    },
  ],
})
export class AppModule {}
