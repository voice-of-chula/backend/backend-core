# Voice of Chula Backend

## Running the backend

1. Install dependecies using `yarn`
2. Start the entirety of the backend using `docker-compose up`.
3. Visit the server using `http://localhost:3000/api`. Explore the available APIs at `http://localhost:3000/api/swagger/`

## Developing

Note that the `.env` is already configured for this operation.

1. Start the Mock SSO using `docker-compose up -d sso`
2. Start the MongoDB using `docker-compose up -d mongo`
3. Install the dependecies using `yarn`
4. Launch the server using `yarn start:dev`

Once done, clean up the containers using `docker-compose down`
